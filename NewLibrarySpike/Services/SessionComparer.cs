using System.Collections.Generic;
using NewLibrarySpike.Data;

namespace NewLibrarySpike.Services
{
    class SessionComparer : IEqualityComparer<SessionWithStrokes>
    {
        public bool Equals(SessionWithStrokes x, SessionWithStrokes y)
        {
            return x.Player.Id == y.Player.Id &&
                   x.Time == y.Time &&
                   x.ClubType == y.ClubType &&
                   x.BallType == y.BallType;
        }

        public int GetHashCode(SessionWithStrokes obj)
        {
            return obj.Player.Id.GetHashCode() ^
                   obj.Time.GetHashCode() ^
                   obj.ClubType.GetHashCode() ^
                   obj.BallType.GetHashCode();
        }
    }
}