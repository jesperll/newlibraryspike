using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text.RegularExpressions;
using NewLibrarySpike.Data;
using NewLibrarySpike.ViewModel;

namespace NewLibrarySpike.Services
{
    [Export]
    class Library
    {
        [Import]
        public IDatabase Database { get; set; }

        IQueryable<Data.DBStroke> GetFilteredStrokes(Func<IQueryable<DBStroke>, IQueryable<DBStroke>> filter = null)
        {
            var strokes = Database.Strokes.Where(x => !x.IsModel);
            if (filter == null)
                return strokes;
            return filter(strokes);
        }

        public DBPlayer GetPlayer(Guid playerId)
        {
            return Database.Players.FirstOrDefault(x => x.Id == playerId);
        }

        public ICollection<PlayerWithStrokeCount> GetPlayersWithStrokeCounts(Func<IQueryable<DBStroke>, IQueryable<DBStroke>> filter = null, bool groupByDate = false)
        {
            ICollection<PlayerWithStrokeCount> pvs = null;
            if (groupByDate)
            {
                pvs = (from s in GetFilteredStrokes(filter)
                       group s by new { date = s.Time.Date, player = s.PlayerId }
                           into ps
                           join p in Database.Players on ps.Key.player equals p.Id
                           orderby ps.Key.date descending, p.Name
                           select
                               new PlayerWithStrokeCount { Group = ps.Key.date.ToShortDateString(), Player = p, StrokeCount = ps.Count() })
                    .ToArray();
            }
            else
            {
                pvs = (from s in GetFilteredStrokes(filter)
                       group s by s.PlayerId
                           into ps
                           join p in Database.Players on ps.Key equals p.Id
                           orderby p.Name
                           select
                               new PlayerWithStrokeCount { Player = p, StrokeCount = ps.Count() })
                    .ToArray();
            }
            return pvs;

        }

        public IEnumerable<Data.DBStroke> GetStrokes(IEnumerable<SessionWithStrokes> sessions)
        {
            return from session in sessions from stroke in session.Strokes select stroke;
        }

        public ICollection<SessionWithStrokes> GetSessionsWithStrokes(Guid playerId, Func<IQueryable<DBStroke>, IQueryable<DBStroke>> filter = null)
        {
            var sessions = (from s in GetFilteredStrokes(filter)
                            where s.PlayerId == playerId
                            join p in Database.Players on s.PlayerId equals p.Id
                            group s by new { Player = p, s.ClubType, s.BallType, s.Time.LocalDateTime.Date }
                                into ss
                                let time = ss.Max(s => s.Time)
                                orderby ss.Key.Player.Name, time descending
                                select new SessionWithStrokes
                                {
                                    Time = ss.Key.Date,
                                    Player = ss.Key.Player,
                                    ClubType = ss.Key.ClubType,
                                    BallType = ss.Key.BallType,
                                    Strokes = ss.ToArray()
                                }).ToArray();
            return sessions;
        }

        public bool NameMatchesFilter(string name, string filter)
        {
            if (string.IsNullOrWhiteSpace(filter))
                return true;
            var nameParts = name.Split(new char[] { ' ', '-' }, StringSplitOptions.RemoveEmptyEntries);
            var initials = new string(nameParts.Select(p => p.First()).ToArray());
            return nameParts.Any(p => p.StartsWith(filter, StringComparison.CurrentCultureIgnoreCase))
                   || initials.StartsWith(filter, StringComparison.CurrentCultureIgnoreCase);
        }

        public PlayerWithStrokeCount GetPlayerWithStrokeCount(Guid playerId)
        {
            return (from s in Database.Strokes
                    where s.PlayerId == playerId
                    group s by s.PlayerId
                        into ps
                        join p in Database.Players on ps.Key equals p.Id
                        orderby p.Name
                        select new PlayerWithStrokeCount { Player = p, StrokeCount = ps.Count() }).FirstOrDefault();
        }
    }
}