using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net.Sockets;
using System.Windows.Media;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Prism.PubSubEvents;
using NewLibrarySpike.Data;

namespace NewLibrarySpike.Services
{
    [Export]
    class StrokeManager : BindableBase, IPartImportsSatisfiedNotification
    {
        [Import]
        IEventAggregator EventAggregator { get; set; }

        [Import]
        IDatabase Database { get; set; }

        public StrokeManager()
        {
            SelectedStrokes = EmptySelection;
            _strokes = new HashSet<DBStroke>();
            Sessions = new SessionWithStrokes[0];
        }

        private readonly HashSet<DBStroke> _strokes = new HashSet<DBStroke>();

        public void LoadStrokes(ICollection<DBStroke> strokes)
        {
            _strokes.UnionWith(strokes);
            UpdateSessions();
            //SetSelection(strokes);
            EventAggregator.GetEvent<LoadedStrokesChangedEvent>().Publish(new LoadedStrokesChangedEvent());
        }

        public void UnloadStrokes(ICollection<DBStroke> strokes)
        {
            SelectedStrokes = SelectedStrokes.Except(strokes).ToArray();
            _strokes.ExceptWith(strokes);
            UpdateSessions();
            var coloredSessions = new HashSet<SessionWithStrokes>(_colorMap.Keys, new SessionComparer());
            coloredSessions.ExceptWith(Sessions);
            foreach (var session in coloredSessions)
            {
                _colorMap.Remove(session);
            }
            EventAggregator.GetEvent<LoadedStrokesChangedEvent>().Publish(new LoadedStrokesChangedEvent());
        }

        private void UpdateSessions()
        {
            var sessions = from s in Strokes
                           join p in Database.Players on s.PlayerId equals p.Id
                           group s by new { Player = p, s.ClubType, s.BallType, s.Time.LocalDateTime.Date }
                               into ss
                               let time = ss.Max(s => s.Time)
                               orderby ss.Key.Player.Name, time descending
                               select ApplyColor(new SessionWithStrokes
                               {
                                   Time = ss.Key.Date,
                                   Player = ss.Key.Player,
                                   ClubType = ss.Key.ClubType,
                                   BallType = ss.Key.BallType,
                                   Strokes = ss.OrderBy(s => s.Time).Select((x, i) =>
                                   {
                                       x.Index = i + 1;
                                       return x;
                                   }).Reverse().ToArray(),
                               });
            Sessions = sessions.ToArray();
        }

        public ICollection<SessionWithStrokes> Sessions { get; private set; }

        public IEnumerable<DBStroke> Strokes
        {
            get { return _strokes; }
        }

        public bool IsCompareMode { get { return CompareStroke != null; } }
        public DBStroke CompareStroke { get; set; }

        public static readonly IReadOnlyCollection<DBStroke> EmptySelection = new ReadOnlyCollection<DBStroke>(new DBStroke[0]);

        public IReadOnlyCollection<DBStroke> SelectedStrokes
        {
            get { return _selectedStrokes; }
            private set
            {
                _selectedStrokes = value;
                OnSelectionChanged();
            }
        }

        public event EventHandler SelectionChanged;

        protected virtual void OnSelectionChanged()
        {
            var handler = SelectionChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        public void ClearSelection()
        {
            SelectedStrokes = EmptySelection;
        }

        public bool ShowAll { get; set; }

        public bool ShowAverages { get; set; }

        public void SetSelection(DBStroke stroke)
        {
            SelectedStrokes = new ReadOnlyCollection<DBStroke>(new[] { stroke });
            ShowAverages = false;
        }

        public void SetSelection(IEnumerable<DBStroke> strokes)
        {
            SelectedStrokes = new ReadOnlyCollection<DBStroke>(strokes.ToArray());
            ShowAverages = true;
        }


        readonly Dictionary<SessionWithStrokes, Color> _colorMap = new Dictionary<SessionWithStrokes, Color>(new SessionComparer());
        private IReadOnlyCollection<DBStroke> _selectedStrokes;

        SessionWithStrokes ApplyColor(SessionWithStrokes session)
        {
            Color color;
            if (!_colorMap.TryGetValue(session, out color))
            {
                var colorCounts = (from c in ColorList
                                   join sc in _colorMap.Values.GroupBy(x => x)
                                       on c equals sc.Key
                                       into temp
                                   from sc in temp.DefaultIfEmpty()
                                   select sc == null ? 0 : sc.Count()
                                   ).ToList();

                var min = colorCounts.Min();
                var minIndex = colorCounts.IndexOf(min);
                color = ColorList[minIndex];
                _colorMap.Add(session, color);
            }
            session.Color = color;
            return session;
        }

        public static readonly Color[] ColorList = new[]
        {
            Colors.White, 
            Colors.Yellow, 
            Colors.Fuchsia, 
            Colors.Cyan, 
            Color.FromArgb(255, 255, 127, 0),
            Colors.Chartreuse, 
            Colors.Red, 
            Color.FromArgb(255, 255, 0, 127), 
            Color.FromArgb(255, 0, 255, 200), 
            Colors.Blue
        };


        public void OnImportsSatisfied()
        {
            UpdateSessions();
        }
    }
}