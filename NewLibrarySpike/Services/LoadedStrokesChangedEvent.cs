using Microsoft.Practices.Prism.PubSubEvents;

namespace NewLibrarySpike.Services
{
    class LoadedStrokesChangedEvent : PubSubEvent<LoadedStrokesChangedEvent>
    {
    }
}