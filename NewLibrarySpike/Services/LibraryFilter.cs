using System;
using System.Collections.Generic;
using System.Linq;
using NewLibrarySpike.Data;
using NewLibrarySpike.Model;

namespace NewLibrarySpike.Services
{
    internal class LibraryFilter
    {
        private readonly HashSet<string> _clubTypes;
        private readonly HashSet<string> _ballTypes;
        private readonly Interval<DateTime>? _timeInterval;

        public LibraryFilter(string clubType, string ballType, DateTime time)
            : this(new[] { clubType }, new[] { ballType }, new Interval<DateTime> { Minimum = time, Maximum = time })
        { }

        public LibraryFilter(IEnumerable<string> clubTypes = null, IEnumerable<string> ballTypes = null, Interval<DateTime>? timeInterval = null)
        {
            _clubTypes = new HashSet<string>(clubTypes ?? Enumerable.Empty<string>());
            _ballTypes = new HashSet<string>(ballTypes ?? Enumerable.Empty<string>());
            _timeInterval = timeInterval;
        }

        public IQueryable<DBStroke> Apply(IQueryable<DBStroke> strokes)
        {
            if (_timeInterval.HasValue)
                strokes = strokes.Where(s => s.Time.LocalDateTime.Date >= _timeInterval.Value.Minimum && s.Time.LocalDateTime.Date <= _timeInterval.Value.Maximum);
            if (_clubTypes.Any())
                strokes = strokes.Where(s => _clubTypes.Contains(s.ClubType));
            if (_ballTypes.Any())
                strokes = strokes.Where(s => _ballTypes.Contains(s.BallType));
            return strokes;
        }
    }
}