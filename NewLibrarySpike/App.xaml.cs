﻿using System;
using System.Windows;

namespace NewLibrarySpike
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            SpikeBootstrapper bootstrapper = new SpikeBootstrapper();
            bootstrapper.Run();
        }

        public ResourceDictionary ThemeDictionary
        {
            // You could probably get it via its name with some query logic as well.
            get { return Resources.MergedDictionaries[0]; }
        }
        public void ChangeTheme(Uri uri)
        {
            ThemeDictionary.MergedDictionaries.Clear();
            ThemeDictionary.MergedDictionaries.Add(new ResourceDictionary() { Source = uri });
        }

        private static bool _isLightTheme = false;

        public static void ToggleTheme()
        {
            _isLightTheme = !_isLightTheme;
            ((App)Application.Current).ChangeTheme(_isLightTheme ?
                new Uri(@"/Resources/Themes/Light.xaml", UriKind.Relative) :
                new Uri(@"/Resources/Themes/Dark.xaml", UriKind.Relative));
        }
    }
}
