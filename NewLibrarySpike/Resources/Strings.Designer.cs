﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NewLibrarySpike.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("NewLibrarySpike.Resources.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back.
        /// </summary>
        public static string Back {
            get {
                return ResourceManager.GetString("Back", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Model Shots.
        /// </summary>
        public static string LibraryModelView {
            get {
                return ResourceManager.GetString("LibraryModelView", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Player.
        /// </summary>
        public static string LibraryPlayerSessionsViewTitle {
            get {
                return ResourceManager.GetString("LibraryPlayerSessionsViewTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search.
        /// </summary>
        public static string LibrarySearchViewTitle {
            get {
                return ResourceManager.GetString("LibrarySearchViewTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shot Series.
        /// </summary>
        public static string SessionsViewTitle {
            get {
                return ResourceManager.GetString("SessionsViewTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Series.
        /// </summary>
        public static string SessionsViewTitleShort {
            get {
                return ResourceManager.GetString("SessionsViewTitleShort", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shots.
        /// </summary>
        public static string StrokesViewTitle {
            get {
                return ResourceManager.GetString("StrokesViewTitle", resourceCulture);
            }
        }
    }
}
