﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using NewLibrarySpike.Data;

namespace NewLibrarySpike
{
    public partial class Avatar : ContentControl
    {
        static Avatar()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Avatar), new FrameworkPropertyMetadata(typeof(Avatar)));
        }

        public static readonly DependencyProperty CornerRadiusProperty = Border.CornerRadiusProperty.AddOwner(typeof(Avatar));

        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty ImageUriProperty = DependencyProperty.Register(
            "ImageUri", typeof(string), typeof(Avatar), new PropertyMetadata(default(string)));

        public string ImageUri
        {
            get { return (string)GetValue(ImageUriProperty); }
            set { SetValue(ImageUriProperty, value); }
        }
    }

    public static class HttpCache
    {
        public static async Task<string> Get(string cacheRoot, string cacheKey, Uri uri, TimeSpan maxAge, Action<string> callback)
        {
            var localFile = Path.Combine(cacheRoot, cacheKey);
            if (File.Exists(localFile))
            {
                callback(localFile);
                if (DateTime.Now - File.GetLastWriteTime(localFile) <= maxAge)
                    return localFile;
            }

            var web = new HttpClient();
            var resp = await web.GetAsync(uri);
            if (!resp.IsSuccessStatusCode)
                return null;
            var s = await resp.Content.ReadAsStreamAsync();
            var tempFile = Path.GetTempFileName();
            using (var fs = File.OpenWrite(tempFile))
            {
                await s.CopyToAsync(fs);
            }
            var folder = Path.GetDirectoryName(localFile);
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            try
            {
                if (File.Exists(localFile))
                    File.Delete(localFile);
                File.Move(tempFile, localFile);
                callback(localFile);
            }
            catch (IOException)
            {

            }
            return localFile;
        }

        private static string GetCachePath(Uri uri)
        {
            return uri.ToString().MD5() + ".jpg";
        }
    }

    public static class Md5Extensions
    {
        // ReSharper disable InconsistentNaming
        public static string MD5(this string value)
        // ReSharper restore InconsistentNaming
        {
            var algorithm = System.Security.Cryptography.MD5.Create();
            var data = Encoding.ASCII.GetBytes(value);
            data = algorithm.ComputeHash(data);
            return data.Aggregate("", (current, t) => current + t.ToString("x2").ToLower());
        }
    }

    public class AvatarConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var email = value as string;
            if (string.IsNullOrWhiteSpace(email))
                return null;
            // Reference: http://en.gravatar.com/site/implement/url
            var sb = new StringBuilder();
            sb.Append("http://www.gravatar.com/avatar/");
            sb.Append(email.ToLowerInvariant().MD5());
            sb.Append(".jpg");

            // Size
            sb.Append("?s=48");

            // Default and special modes
            sb.AppendFormat("&d=404");

            const string baseUrl = "http://services.mytrackman.com/File.aspx";

            var avatarUri = new Uri(string.Format("{0}?avatarToken={1}&redirectUrl={2}", baseUrl, TempLoginToken.GetTempToken(email), Uri.EscapeDataString(sb.ToString())));
            return avatarUri;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ImageCache
    {
        private static readonly Random Random = new Random();

        public static readonly DependencyProperty SourceUriProperty = DependencyProperty.RegisterAttached(
            "SourceUri", typeof(string), typeof(ImageCache), new PropertyMetadata(default(string), SourceUriChanged));

        static async Task GetAvatarImage(Image image, string uri)
        {
            Task<string> fileTask;
            if (MemCache.TryGetValue(uri, out fileTask))
            {
                image.Source = LoadImage(await fileTask);
                return;
            }

            var localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var cacheRoot = Path.Combine(localAppData, "AvatarCache");
            MemCache[uri] = HttpCache.Get(cacheRoot, uri.MD5() + ".jpg", new Uri(uri), TimeSpan.FromMinutes(5), file =>
            {
                image.Source = LoadImage(file);
            });
        }

        static readonly Dictionary<string, Task<string>> MemCache = new Dictionary<string, Task<string>>();

        private async static void SourceUriChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var email = (string)e.NewValue;
            var image = (Image)d;
            if (string.IsNullOrWhiteSpace(email))
                image.Source = null;
            else
                await GetAvatarImage(image, email);
        }

        static BitmapImage LoadImage(string path)
        {
            if (path == null)
                return null;
            var bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.CacheOption = BitmapCacheOption.OnLoad;
            bmp.UriSource = new Uri(path);
            bmp.EndInit();
            return bmp;
        }
        public static void SetSourceUri(Image element, string value)
        {
            element.SetValue(SourceUriProperty, value);
        }

        public static string GetSourceUri(Image element)
        {
            return (string)element.GetValue(SourceUriProperty);
        }
    }

    public static class TempLoginToken
    {
        private const string Passkey = "MothersTrackMan";

        public static string GetTempLoginToken(Guid playerId)
        {
            return GetTempToken(playerId.ToString());
        }

        public static string GetTempToken(string message)
        {
            return Uri.EscapeDataString(EncryptData(message + "|" + DateTime.UtcNow.Ticks));
        }

        public static Tuple<bool, DateTime, Guid> ValidateLoginToken(string token)
        {
            var validate = ValidateToken(token);
            if (validate != null)
            {
                Guid id;
                if (Guid.TryParse(validate.Item3, out id))
                {
                    return new Tuple<bool, DateTime, Guid>(validate.Item1, validate.Item2, id);
                }
            }
            return new Tuple<bool, DateTime, Guid>(false, new DateTime(), Guid.Empty);
        }

        public static Tuple<bool, DateTime, string> ValidateToken(string token)
        {
            var oldMessge = DecryptString(Uri.UnescapeDataString(token));
            if (oldMessge == null)
                return new Tuple<bool, DateTime, string>(false, new DateTime(), null);

            var splitIndex = oldMessge.LastIndexOf("|", StringComparison.Ordinal);
            if (splitIndex > -1)
            {
                var message = oldMessge.Substring(0, splitIndex);
                long ticks;
                if (long.TryParse(oldMessge.Substring(splitIndex + 1), out ticks))
                {
                    var time = new DateTime(ticks, DateTimeKind.Utc);
                    var ok = (DateTime.UtcNow - time) < TimeSpan.FromDays(1);
                    return new Tuple<bool, DateTime, string>(ok, time, message);
                }
            }

            return null;
        }

        public static string EncryptData(string message)
        {
            byte[] results;
            var utf8 = new UTF8Encoding();
            var hashProvider = new MD5CryptoServiceProvider();
            byte[] tdesKey = hashProvider.ComputeHash(utf8.GetBytes(Passkey));
            var tdesAlgorithm = new TripleDESCryptoServiceProvider { Key = tdesKey, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 };
            byte[] dataToEncrypt = utf8.GetBytes(message);
            try
            {
                ICryptoTransform encryptor = tdesAlgorithm.CreateEncryptor();
                results = encryptor.TransformFinalBlock(dataToEncrypt, 0, dataToEncrypt.Length);
            }
            finally
            {
                tdesAlgorithm.Clear();
                hashProvider.Clear();
            }
            return Convert.ToBase64String(results);
        }

        public static string DecryptString(string message)
        {
            try
            {
                byte[] results;
                var utf8 = new UTF8Encoding();
                var hashProvider = new MD5CryptoServiceProvider();
                byte[] tdesKey = hashProvider.ComputeHash(utf8.GetBytes(Passkey));
                var tdesAlgorithm = new TripleDESCryptoServiceProvider { Key = tdesKey, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 };
                byte[] dataToDecrypt = Convert.FromBase64String(message);
                try
                {
                    ICryptoTransform decryptor = tdesAlgorithm.CreateDecryptor();
                    results = decryptor.TransformFinalBlock(dataToDecrypt, 0, dataToDecrypt.Length);
                }
                finally
                {
                    tdesAlgorithm.Clear();
                    hashProvider.Clear();
                }
                return utf8.GetString(results);
            }
            catch
            {
                return null;
            }
        }
    }
}
