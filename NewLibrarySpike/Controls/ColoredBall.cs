﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace NewLibrarySpike.Controls
{
    public class ColoredBall : Control
    {
        static ColoredBall()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ColoredBall), new FrameworkPropertyMetadata(typeof(ColoredBall)));
        }

        public static readonly DependencyProperty ColorProperty = DependencyProperty.Register(
            "Color", typeof(Color), typeof(ColoredBall), new FrameworkPropertyMetadata(default(Color)));

        public Color Color
        {
            get { return (Color)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        public static readonly DependencyProperty IsGroupProperty = DependencyProperty.Register(
            "IsGroup", typeof(bool), typeof(ColoredBall), new PropertyMetadata(default(bool)));

        public bool IsGroup
        {
            get { return (bool)GetValue(IsGroupProperty); }
            set { SetValue(IsGroupProperty, value); }
        }
    }
}
