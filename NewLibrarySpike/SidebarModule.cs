using System;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using NewLibrarySpike.Views;

namespace NewLibrarySpike
{
    [ModuleExport(typeof(SidebarModule))]
    public class SidebarModule : IModule
    {
        [Import]
        public IRegionManager RegionManager;

        public void Initialize()
        {
            //this.RegionManager.RegisterViewWithRegion(RegionNames.SidebarRegion, typeof(SessionsView));
            //this.RegionManager.RequestNavigate(RegionNames.SidebarRegion, new Uri("/SessionsView", UriKind.Relative));
        }
    }
    
    [ModuleExport(typeof(StatusModule))]
    public class StatusModule : IModule
    {
        [Import]
        public IRegionManager RegionManager;

        public void Initialize()
        {
            this.RegionManager.RegisterViewWithRegion(RegionNames.MainRegion, typeof(StatusView));
        }
    }
}