using System;
using System.Diagnostics;

namespace NewLibrarySpike.Data
{
    [DebuggerDisplay("{DebuggerDisplay,nq}")]
    public class DBPlayer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string ProfileImageUri { get; set; }
        public string Nationality { get; set; }
        public DateTime? Birthday { get; set; }
        public int? Hcp { get; set; }
        string DebuggerDisplay { get { return string.Format("{0} <{1}>", Name, Email); } }
    }
}

