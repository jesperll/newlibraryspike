using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace NewLibrarySpike.Data
{
    [DebuggerDisplay("{DebuggerDisplay,nq}")]
    class SessionWithStrokes
    {
        public DateTime Time { get; set; }
        public DBPlayer Player { get; set; }
        public string ClubType { get; set; }
        public string BallType { get; set; }
        public ICollection<DBStroke> Strokes { get; set; }
        public Color Color { get; set; }
        string DebuggerDisplay { get { return string.Format("{0} {1:yyyy MMM dd} {2} {3} {4} ({5})", Player.Name, Time, ClubType, BallType, Strokes.Any(s => s.VideoCount > 0) ? "V" : "-", Strokes.Count); } }
        public int VideoCount { get { return Strokes == null ? 0 : Strokes.Sum(x => x.VideoCount); } }
    }
    /*

    struct SessionKey : IEquatable<SessionKey>
    {
        public DateTime Time { get; set; }
        public Guid PlayerId { get; set; }
        public string ClubType { get; set; }
        public string BallType { get; set; }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Time.GetHashCode();
                hashCode = (hashCode*397) ^ PlayerId.GetHashCode();
                hashCode = (hashCode*397) ^ (ClubType != null ? ClubType.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (BallType != null ? BallType.GetHashCode() : 0);
                return hashCode;
            }
        }

        public bool Equals(SessionKey other)
        {
            return Time.Equals(other.Time) && PlayerId.Equals(other.PlayerId) && string.Equals(ClubType, other.ClubType) && string.Equals(BallType, other.BallType);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is SessionKey && Equals((SessionKey) obj);
        }
    }

    class LoadedSession
    {
        public SessionKey Key { get; set; }
        public Color Color { get; set; }
        public string PlayerName { get; set; }
        public string PlayerEmail { get; set; }
        public ICollection<DBStroke> Strokes { get; set; }
    }*/
}