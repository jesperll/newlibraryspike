using System;
using System.Diagnostics;

namespace NewLibrarySpike.Data
{
    [DebuggerDisplay("{DebuggerDisplay,nq}")]
    class PlayerWithStrokeCount
    {     
        public string Group { get; set; }
        public DBPlayer Player { get; set; }
        public int StrokeCount { get; set; }
        string DebuggerDisplay { get { return string.Format("{0} <{1}> ({2})", Player.Name, Player.Email, StrokeCount); } }
    }
}