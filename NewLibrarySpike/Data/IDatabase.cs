using System.Linq;

namespace NewLibrarySpike.Data
{
    interface IDatabase
    {
        IQueryable<DBStroke> Strokes { get; }
        IQueryable<DBPlayer> Players { get; }
    }
}