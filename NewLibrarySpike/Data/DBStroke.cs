using System;
using System.Diagnostics;

namespace NewLibrarySpike.Data
{
    [DebuggerDisplay("{DebuggerDisplay,nq}")]
    class DBStroke
    {
        public Guid Id { get; set; }
        public int Index { get; set; }
        public bool IsModel { get; set; }
        public bool IsStarred { get; set; }
        public bool Ignore { get; set; }
        public DateTimeOffset Time { get; set; }
        public Guid PlayerId { get; set; }
        public string ClubType { get; set; }
        public string BallType { get; set; }
        public double DataValue { get; set; }
        public int VideoCount { get; set; }
        string DebuggerDisplay { get { return string.Format("{0},{1} {2}", ClubType, BallType, DataValue); } }
    }
}