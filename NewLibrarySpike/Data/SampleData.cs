using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using FileHelpers;
using NewLibrarySpike.Model;
using NewLibrarySpike.Properties;
using NewLibrarySpike.ViewModel.Filter;
using Newtonsoft.Json;
using Office;

namespace NewLibrarySpike.Data
{
    [Export(typeof(IDatabase))]
    class SampleData : IDatabase
    {
        static readonly Random Random = new Random(0);
        private readonly List<DBPlayer> _players = new List<DBPlayer>();
        private readonly List<DBStroke> _strokes = new List<DBStroke>();
        public SampleData()
        {
            //var kej = AddPlayer("Klaus Eldrup-J�rgensen", "kej@trackman.dk");
            //AddSession(new DateTime(2014, 3, 15, 12, 43, 0), kej.Id, "6i", "limH", 1, 5);
            //var ft = AddPlayer("Fredrik Tuxen", "ft@trackman.dk");
            //AddSession(new DateTime(2013, 10, 13, 14, 3, 0), ft.Id, "6i", "limH", 2, 13);
            //AddSession(new DateTime(2014, 3, 31, 11, 12, 0), ft.Id, "Dr", "Pre", 0, 28);
            //AddSession(new DateTime(2014, 3, 31, 16, 31, 0), ft.Id, "6i", "limH", 2, 8);

            //AddSession(new DateTime(2013, 11, 13, 13, 6, 0), kej.Id, "Dr", "Pre", 1, 12);

            //AddSession(new DateTime(2014, 3, 15, 12, 0, 0), ft.Id, "PW", "limH", 2, 53);
            //AddSession(new DateTime(2014, 2, 8, 12, 0, 0), ft.Id, "8i", "Pre", 0, 28);
            //AddSession(new DateTime(2013, 12, 22, 12, 0, 0), ft.Id, "Dr", "LimH", 0, 28);

            //var ib = AddPlayer("Ib Frederiksen", "ib@example.org");
            //AddSession(new DateTime(2013, 12, 16, 12, 0, 0), ib.Id, "Dr", "LimH", 0, 5);
            //AddSession(new DateTime(2013, 12, 16, 11, 0, 0), ib.Id, "Hy", "LimH", 0, 5);
            //var cf = AddPlayer("Carl Frenchie", "carl@example.org");
            //AddSession(new DateTime(2013, 12, 14, 12, 0, 0), cf.Id, "6i", "LimH", 0, 15);

            var clubs = new ClubFilterViewModel().ClubTypeGroups.SelectMany(x => x.ClubTypes).Select(x => x.ShortName).ToArray();

            try
            {
                var a = ZipFile.OpenRead("Data/FakeUsers.zip");
                var e = a.GetEntry("FakeUsers.csv");
                var s = e.Open();
                var t = new StreamReader(s, Encoding.UTF8);
                var engine = new FileHelperEngine<FakeUser>();
                bool header = true;
                engine.BeforeReadRecord += (@base, args) =>
                {
                    args.SkipThisRecord = header;
                    header = false;
                };
                var users = engine.ReadStream(t, 234);
                int m = 0, f = 0;
                var players = from u in users
                              select new DBPlayer
                              {
                                  Id = u.GUID,
                                  Name = u.GivenName + " " + u.Surname,
                                  Nationality = u.Country,
                                  Birthday = u.Birthday,
                                  Email = u.EmailAddress,
                                  Hcp = Random.Next(-5, 36),
                                  Gender = u.Gender,
                                  ProfileImageUri = Random.Next(100) < 10 ? null : u.Gender == "male" ?
                                    string.Format("http://api.randomuser.me/portraits/thumb/men/{0}.jpg", (m++) % 100) :
                                    string.Format("http://api.randomuser.me/portraits/thumb/women/{0}.jpg", (f++) % 100)
                              };
                _players = players.ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _players.Add(new DBPlayer
                {
                    Id = Guid.NewGuid(),
                    Name = "Jesper Larsen-Ledet",
                    Nationality = "Denmark",
                    Birthday = new DateTime(1974, 6, 16),
                    Email = "jll@trackman.dk",
                    Hcp = 36,
                    Gender = "male"
                });
            }
            foreach (var p in _players)
            {
                var sessioncount = Random.Next(1, 100);
                for (int i = 0; i < sessioncount; i++)
                {
                    AddSession(DateTime.Now - TimeSpan.FromMinutes(Random.Next(60 * 24 * 365 * 4)), p.Id, Random.NextItem(clubs), "Pre", Random.Next(5) == 0 ? 0 : Random.Next(1, 4), Random.Next(Random.Next(10) == 0 ? 1 : 30, 60));
                }
            }

        }
        class GuidConverter : ConverterBase
        {
            public override object StringToField(string from)
            {
                return new Guid(from);
            }
        }
        class DateConverter : ConverterBase
        {
            public override object StringToField(string from)
            {
                return DateTime.ParseExact(from, "M/d/yyyy", CultureInfo.InvariantCulture);
            }
        }

        [DelimitedRecord(",")]
        class FakeUser
        {
            [FieldConverter(typeof(GuidConverter))]
            public Guid GUID;
            public string Gender;
            public string GivenName;
            public string Surname;
            public string EmailAddress;
            public string Country;
            public string CountryFull;
            [FieldConverter(typeof(DateConverter))]
            public DateTime Birthday;
            public double Kilograms;
            public double Centimeters;
            public string TelephoneNumber;
        }

        private void AddSession(DateTimeOffset start, Guid playerId, string club, string ball, int videosPerStroke, int count)
        {
            for (int i = 0; i < count; i++)
            {
                var stroke = new DBStroke
                {
                    Id = Guid.NewGuid(),
                    PlayerId = playerId,
                    IsModel = count == 1,
                    IsStarred = Random.Next(1000) == 0,
                    Ignore = Random.Next(500) == 0,
                    ClubType = club,
                    BallType = ball,
                    VideoCount = Random.Next(0, 1 + videosPerStroke),
                    DataValue = Random.Next(890, 1500) * 0.1,
                    Time = start + TimeSpan.FromSeconds(45 * i),
                };
                _strokes.Add(stroke);
            }
        }

        private DBPlayer AddPlayer(string name, string email)
        {
            var player = new DBPlayer { Id = Guid.NewGuid(), Name = name, Email = email };
            _players.Add(player);
            return player;
        }

        public IQueryable<DBStroke> Strokes { get { return _strokes.AsQueryable(); } }
        public IQueryable<DBPlayer> Players { get { return _players.AsQueryable(); } }
    }

    public static class RandomExtension
    {
        public static T NextItem<T>(this Random random, IList<T> list)
        {
            return list[random.Next(list.Count - 1)];
        }
    }

}