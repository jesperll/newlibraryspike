﻿using System;

namespace NewLibrarySpike
{
    public static class RegionNames
    {
        public const String MainRegion = "MainRegion";
        public const String SidebarRegion = "SidebarRegion";
        public const String SidebarContentRegion = "SidebarContentRegion";
    }
}