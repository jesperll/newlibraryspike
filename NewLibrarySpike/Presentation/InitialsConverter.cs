﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using System.Windows.Markup;

namespace NewLibrarySpike.Presentation
{
    public class InitialsConverter : MarkupExtension, IValueConverter
    {
        // ReSharper disable once EmptyConstructor
        public InitialsConverter() { }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var n = value as string;
            if (string.IsNullOrWhiteSpace(n))
                return null;
            var initials = from name in n.Split(' ', '-')
                           where Char.IsLetter(name[0])
                           select name[0];
            return new string(initials.ToArray()).ToUpper();

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}