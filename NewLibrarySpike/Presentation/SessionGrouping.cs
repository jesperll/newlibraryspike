using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using NewLibrarySpike.Data;

namespace NewLibrarySpike.Presentation
{
    public struct SessionHeader
    {
        public Guid PlayerId { get; set; }
        public string PlayerName { get; set; }
        public string PlayerEmail { get; set; }
        public DBPlayer Player { get; set; }
    }

    public class SessionGrouping : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var session = value as SessionWithStrokes;
            if (session == null)
                return DependencyProperty.UnsetValue;
            return new SessionHeader { PlayerId = session.Player.Id, PlayerName = session.Player.Name, PlayerEmail = session.Player.Email, Player = session.Player };
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SessionLegend : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var session = value as SessionWithStrokes;
            if (session == null)
                return DependencyProperty.UnsetValue;
            return string.Format("{0:yyyy MMM dd} {1} {2}", session.Time, session.ClubType, session.BallType);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class PlayerSessionLegend : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var session = value as SessionWithStrokes;
            if (session == null)
                return DependencyProperty.UnsetValue;
            return string.Format("{3}, {0:yyyy MMM dd} {1} {2}", session.Time, session.ClubType, session.BallType, session.Player.Name.GetInitials());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public static class StringHelper
    {
        public static string GetInitials(this string name)
        {
            var nameParts = name.Split(new char[] { ' ', '-' }, StringSplitOptions.RemoveEmptyEntries);
            return new string(nameParts.Select(p => p.First()).ToArray()).ToUpper();

        }
    }
}