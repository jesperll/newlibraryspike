using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace NewLibrarySpike.Presentation
{
    public class CommandBindingBehavior : Behavior<FrameworkElement>
    {
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(CommandBindingBehavior), new PropertyMetadata(default(ICommand)));

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly DependencyProperty TargetCommandProperty =
            DependencyProperty.Register("TargetCommand", typeof(ICommand), typeof(CommandBindingBehavior), new PropertyMetadata(default(ICommand)));

        public ICommand TargetCommand
        {
            get { return (ICommand)GetValue(TargetCommandProperty); }
            set { SetValue(TargetCommandProperty, value); }
        }

        private CommandBinding _commandBinding;

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.CommandBindings.Add(_commandBinding = new CommandBinding(Command, Executed, CanExecute));
        }
        protected override void OnDetaching()
        {
            base.OnDetaching();
            if (_commandBinding != null)
                AssociatedObject.CommandBindings.Remove(_commandBinding);
        }

        private void Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (TargetCommand != null)
                TargetCommand.Execute(e.Parameter);
        }

        private void CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = TargetCommand != null && TargetCommand.CanExecute(e.Parameter);
        }
    }
}