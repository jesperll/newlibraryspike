﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace NewLibrarySpike.Presentation
{
    public class HcpConverter : MarkupExtension, IValueConverter
    {
// ReSharper disable once EmptyConstructor
        public HcpConverter(){}

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var nhcp = value as int?;
            if (!nhcp.HasValue)
                return "?";
            var hcp = nhcp.Value;
            if (hcp < 0)
                return "+" + (-hcp);
            return hcp.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}