using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace NewLibrarySpike.Presentation
{
    public class CountToVisibilityConverter : IValueConverter
    {
        public Visibility GreaterThanVisibility { get; set; }
        public Visibility LessThanOrEqualVisibility { get; set; }
        public int Value { get; set; }

        public CountToVisibilityConverter()
        {
            GreaterThanVisibility = Visibility.Visible;
            LessThanOrEqualVisibility = Visibility.Collapsed;
        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToInt32(value) > Value ? GreaterThanVisibility : LessThanOrEqualVisibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}