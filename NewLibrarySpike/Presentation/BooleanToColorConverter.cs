using System.Windows.Media;

namespace NewLibrarySpike.Presentation
{
    public class BooleanToColorConverter : BooleanConverter<Color>
    {
        public BooleanToColorConverter()
        {
            False = Colors.Black;
            True = Colors.White;
        }
    }
}