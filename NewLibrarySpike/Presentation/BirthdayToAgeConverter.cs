﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace NewLibrarySpike.Presentation
{
    public class BirthdayToAgeConverter : MarkupExtension, IValueConverter
    {
        // ReSharper disable once EmptyConstructor
        public BirthdayToAgeConverter() { }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var birthday = value as DateTime?;
            if (birthday == null)
                return null;
            var today = DateTime.Today;
            var age = today.Year - birthday.Value.Year;
            if (birthday.Value > today.AddYears(-age)) age--;
            return age;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
