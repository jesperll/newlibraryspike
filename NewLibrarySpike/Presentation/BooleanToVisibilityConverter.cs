using System.Windows;

namespace NewLibrarySpike.Presentation
{
    public class BooleanToVisibilityConverter : BooleanConverter<Visibility>
    {
        public BooleanToVisibilityConverter()
        {
            False = Visibility.Collapsed;
            True = Visibility.Visible;
        }
    }
}