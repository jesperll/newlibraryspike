using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace NewLibrarySpike.Presentation
{
    public class StringNullOrEmptyToVisibilityConverter : MarkupExtension, IValueConverter
    {
        public Visibility NonNullOrEmptyValue { get; set; }
        public Visibility NullOrEmptyValue { get; set; }

        public StringNullOrEmptyToVisibilityConverter()
        {
            NonNullOrEmptyValue = Visibility.Visible;
            NullOrEmptyValue = Visibility.Collapsed;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = value as string;
            return string.IsNullOrEmpty(s) ? NullOrEmptyValue : NonNullOrEmptyValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}