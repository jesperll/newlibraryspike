using System.Windows;
using System.Windows.Controls.Primitives;

namespace NewLibrarySpike.Presentation
{
    public class ButtonHelper
    {
        public static readonly DependencyProperty IsCheckedProperty =
            DependencyProperty.RegisterAttached("IsChecked", typeof(bool?), typeof(ButtonHelper));

        public static bool? GetIsChecked(ButtonBase obj)
        {
            return (bool?)obj.GetValue(IsCheckedProperty);
        }
        public static void SetIsChecked(ButtonBase obj, bool? value)
        {
            obj.SetValue(IsCheckedProperty, value);
        }
    }
}