using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace NewLibrarySpike.Presentation
{
    public abstract class ValueConverter : MarkupExtension, IValueConverter
    {
        public sealed override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public virtual object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public abstract class BooleanConverter<T> : ValueConverter
    {
        public T True { get; set; }
        public T False { get; set; }

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToBoolean(value) ? True : False;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Equals(value, True);
        }
    }

    public class BooleanToDoubleConverter : BooleanConverter<double>
    {
    }

    public class BooleanNotConverter : ValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !System.Convert.ToBoolean(value);
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !System.Convert.ToBoolean(value);
        }
    }

    /// <summary>
    /// Returns a different value if used in design mode
    /// </summary>
    public class DesignTimeConverter : ValueConverter
    {
        static readonly Lazy<bool> IsInDesignMode = new Lazy<bool>(() =>
            (bool)DependencyPropertyDescriptor.FromProperty(
                        DesignerProperties.IsInDesignModeProperty,
                        typeof(FrameworkElement))
                        .Metadata
                        .DefaultValue);

        // ReSharper disable once EmptyConstructor
        public DesignTimeConverter() { }

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return IsInDesignMode.Value ? parameter : value;
        }
    }
}