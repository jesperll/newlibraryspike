namespace NewLibrarySpike.Presentation
{
    public class BooleanToStringConverter : BooleanConverter<string>
    {
        public BooleanToStringConverter()
        {
            False = "False";
            True = "True";
        }
    }
}