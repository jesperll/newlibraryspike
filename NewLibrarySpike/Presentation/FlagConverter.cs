﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace NewLibrarySpike.Presentation
{
    public class FlagConverter : MarkupExtension, IValueConverter
    {
        // ReSharper disable once EmptyConstructor
        public FlagConverter() { }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var n = value as string;
            if (string.IsNullOrEmpty(n))
                n = "_unknown";
            return "pack://application:,,,/NewLibrarySpike;component/Resources/Flags/" + n + ".png";

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}