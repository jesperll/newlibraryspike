using System.Windows.Controls;
using NewLibrarySpike.Presentation;

namespace NewLibrarySpike
{
    public class SelectionModeConverter : BooleanConverter<SelectionMode>
    {
        public SelectionModeConverter()
        {
            False = SelectionMode.Single;
            True = SelectionMode.Multiple;
        }
    }
}