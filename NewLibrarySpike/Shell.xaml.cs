﻿using System;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;

namespace NewLibrarySpike
{
    [Export]
    public partial class Shell : Window, IPartImportsSatisfiedNotification
    {
        public Shell()
        {
            InitializeComponent();
        }

        [Import(AllowRecomposition = false)]
        public IModuleManager ModuleManager;

        [Import(AllowRecomposition = false)]
        public IRegionManager RegionManager;


        public void OnImportsSatisfied()
        {
#if DEBUG
            this.RegionManager.RequestNavigate(RegionNames.SidebarRegion, new Uri("/SidebarView", UriKind.Relative));
            this.RegionManager.RequestNavigate(RegionNames.SidebarContentRegion, new Uri("/StartView", UriKind.Relative));
#else
            this.RegionManager.RequestNavigate(RegionNames.SidebarRegion, new Uri("/SessionsView", UriKind.Relative));
            this.RegionManager.RequestNavigate(RegionNames.SidebarRegion, new Uri("/LibrarySearchView", UriKind.Relative));
#endif
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.F11)
                App.ToggleTheme();
            base.OnPreviewKeyDown(e);
        }

        //protected override void OnPreviewStylusDown(StylusDownEventArgs e)
        //{
        //    base.OnPreviewStylusDown(e);
        //}

        //protected override void OnPreviewStylusUp(StylusEventArgs e)
        //{
        //    base.OnPreviewStylusUp(e);

        //    // move cursor to center of title bar to avoid IsMouseOver triggers on the element that was touched
        //    var dest = this.PointToScreen(new Point(this.Width / 2, 0));
        //    Win32.SetCursorPos((int)dest.X, (int)dest.Y);
        //}
    }

    public class Win32
    {
        [DllImport("User32.Dll")]
        public static extern long SetCursorPos(int x, int y);
    }
}
