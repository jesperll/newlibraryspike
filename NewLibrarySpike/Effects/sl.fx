sampler2D input : register(s0);

/// <minValue>0/minValue>
/// <maxValue>200</maxValue>
/// <defaultValue>100</defaultValue>
float Saturation : register(C0);

/// <minValue>-100/minValue>
/// <maxValue>100</maxValue>
/// <defaultValue>50</defaultValue>
float Lightness : register(C1);

float4 main(float2 uv : TEXCOORD) : COLOR 
{ 
	 // some vars
   float saturation = Saturation / 100 ;
   float lightness = Lightness / 100;
   float3  luminanceWeights = float3(0.299,0.587,0.114);

    // input raw pixel
    float4 srcPixel = tex2D(input, uv);

    // Undo pre-multiplied alpha.
    float3 dstPixel = srcPixel.rgb / srcPixel.a;
    
    // Apply saturation
    float luminance = dot(dstPixel, luminanceWeights);
    dstPixel = lerp(luminance, dstPixel, saturation);

    // Apply lightness
    dstPixel += lightness;

    return float4(dstPixel, srcPixel.a);
}