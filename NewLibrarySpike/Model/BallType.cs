namespace NewLibrarySpike.Model
{
    class BallType
    {
        public string ShortName { get; set; }
        public string Name { get; set; }
    }
}