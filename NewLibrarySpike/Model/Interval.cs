﻿namespace NewLibrarySpike.Model
{
    struct Interval<T>
    {
        public T Minimum { get; set; }
        public T Maximum { get; set; }
    }
}