namespace NewLibrarySpike.Model
{
    class ClubType
    {
        private readonly string _name;
        private readonly string _shortName;

        public ClubType(string name, string shortName)
        {
            _name = name;
            _shortName = shortName;
        }

        public string Name
        {
            get { return _name; }
        }

        public string ShortName
        {
            get { return _shortName; }
        }
    }
}