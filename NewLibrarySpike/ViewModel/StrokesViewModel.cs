using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Prism.Regions;
using NewLibrarySpike.Data;
using NewLibrarySpike.Services;

namespace NewLibrarySpike.ViewModel
{

    [Export]
    class StrokesViewModel : NavigationAwareViewModel
    {
        [Import]
        StrokeManager StrokeManager { get; set; }

        public SessionViewModel Session { get; set; }
        public IList<StrokeViewModel> Strokes { get; set; }

        public ICommand ShowAveragesCommand { get { return new DelegateCommand(ShowAverages); } }

        public ICommand ShowSingleCommand { get { return new DelegateCommand<StrokeViewModel>(ShowSingle); } }

        public ICommand ToggleSelectionCommand { get { return new DelegateCommand<StrokeViewModel>(ToggleSelection); } }

        private void ShowAverages()
        {
            Session.IsSelected = true;
            foreach (var stroke in Strokes)
            {
                stroke.IsSelected = false;
            }
            StrokeManager.SetSelection(Strokes.Where(x => x.IsChecked).Select(x => x.Stroke));
        }

        private void ToggleSelection(StrokeViewModel stroke)
        {
            stroke.IsChecked = !stroke.IsChecked;
            ShowAverages();
        }

        private void ShowSingle(StrokeViewModel stroke)
        {
            Session.IsSelected = false;
            foreach (var model in Strokes)
            {
                model.IsSelected = model == stroke;
            }
            StrokeManager.SetSelection(stroke.Stroke);
        }

        public ICommand ModifyStrokesCommand
        {
            get { return new DelegateCommand(ModifyStrokes); }
        }

        private void ModifyStrokes()
        {
            var strokes = GetSelectedStrokes();

            MessageBox.Show(strokes.Length.Pluralize("Modifying 1 stroke", "Modifying {0} strokes"));
        }

        private DBStroke[] GetSelectedStrokes()
        {
            if (Session.IsSelected)
                return Strokes.Where(x => x.IsChecked).Select(x => x.Stroke).ToArray();
            return Strokes.Where(x => x.IsSelected).Select(x => x.Stroke).ToArray();
        }

        public ICommand CompareCommand
        {
            get { return new DelegateCommand<StrokeViewModel>(Compare); }
        }

        private void Compare(StrokeViewModel stroke)
        {
            NavigationService.RequestNavigate(new Uri("/CompareView", UriKind.Relative));
        }

        protected override void OnNavigatedTo(NavigationParameters parameters)
        {
            var playerId = Guid.Parse((string)parameters["PlayerId"]);
            var time = DateTime.Parse((string)parameters["Time"]);
            var club = (string)parameters["ClubType"];
            var ball = (string)parameters["BallType"];

            var c = new SessionComparer();
            var dummysession = new SessionWithStrokes
            {
                Player = new DBPlayer { Id = playerId },
                Time = time,
                ClubType = club,
                BallType = ball
            };
            Session = new SessionViewModel(StrokeManager.Sessions.First(x => c.Equals(x, dummysession)));
            Strokes = Session.Strokes.OrderBy(s => s.Time).Select((s, i) => new StrokeViewModel(s, i + 1)).Reverse().ToArray();
            StrokeManager.ShowAll = false;
            var st = Strokes.FirstOrDefault(x => x.IsChecked);
            if (st != null)
                ShowSingle(st);
        }

        public void SelectNext()
        {
            var i = Strokes.IndexOf(true, x => x.IsSelected);
            if (i > 0)
                ShowSingle(Strokes[i - 1]);
            else
                ShowAverages();
        }

        public void SelectPrevious()
        {
            var i = Strokes.IndexOf(true, x => x.IsSelected);
            if (i < 0)
                ShowSingle(Strokes[0]);
            else if (i < Strokes.Count - 1)
                ShowSingle(Strokes[i + 1]);

        }
    }

    class SessionViewModel : BindableBase
    {
        private readonly SessionWithStrokes _session;

        public bool IsSelected { get; set; }

        public SessionViewModel(SessionWithStrokes session)
        {
            _session = session;
        }

        public SessionWithStrokes Session
        {
            get { return _session; }
        }
        public int Count { get { return Strokes.Count(x => !x.Ignore); } }
        public ICollection<DBStroke> Strokes { get { return _session.Strokes; } }
    }


    class StrokeViewModel : BindableBase
    {
        private readonly DBStroke _stroke;
        private readonly int _index;

        public bool IsChecked
        {
            get { return !_stroke.Ignore; }
            set { _stroke.Ignore = !value; }
        }

        public bool IsSelected { get; set; }

        public StrokeViewModel(DBStroke stroke, int index)
        {
            _stroke = stroke;
            _index = index;
        }

        public DBStroke Stroke
        {
            get { return _stroke; }
        }

        public int Index
        {
            get { return _index; }
        }

        public string Data { get { return string.Format("{0:0.0}", _stroke.DataValue); } }
        public string Unit { get { return "yds"; } }

        public string[] Videos
        {
            get { return new string[_stroke.VideoCount]; }
        }

        public bool IsFavorite { get; set; }
        public bool IsIgnored { get; set; }
    }
}