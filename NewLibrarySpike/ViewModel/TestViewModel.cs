﻿using System.ComponentModel.Composition;
using System.Reflection;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;

namespace NewLibrarySpike.ViewModel
{
    [Export]
    public class TestViewModel : NavigationAwareViewModel
    {
        public TestViewModel()
        {
            ShotOfShots = "6 of 24";
            Target = "85 m";
            Score = 46.8;
            LastShot = new TestSummaryLine(1, "75 m", 49.8, 15.4, 60.5, null);
            Summary = new[]
            {
                new TestSummaryLine(3, "60 m", 35.2, 16.2, 60.5, null),
                new TestSummaryLine(2, "75 m", 45.2, 13.2, 78.5, null),
            };
        }

        protected override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            IsStarted = false;
        }

        public bool IsStarted { get; private set; }
        public ICommand StartCommand { get { return new DelegateCommand(() => IsStarted = true); } }

        public string ShotOfShots { get; set; }

        public string Target { get; set; }

        public double Score { get; set; }

        public TestSummaryLine LastShot { get; set; }
        public TestSummaryLine[] Summary { get; set; }
    }

    public class TestSummaryLine
    {
        private readonly int _shots;
        private readonly string _target;
        private readonly double _score;
        private readonly double _fromPin;
        private readonly double? _carry;
        private readonly double? _total;

        public TestSummaryLine(int shots, string target, double score, double fromPin, double? carry, double? total)
        {
            _shots = shots;
            _target = target;
            _score = score;
            _fromPin = fromPin;
            _carry = carry;
            _total = total;
        }

        public int Shots
        {
            get { return _shots; }
        }

        public string Target
        {
            get { return _target; }
        }

        public double Score
        {
            get { return _score; }
        }

        public double FromPin
        {
            get { return _fromPin; }
        }

        public double? Carry
        {
            get { return _carry; }
        }

        public double? Total
        {
            get { return _total; }
        }
    }
}