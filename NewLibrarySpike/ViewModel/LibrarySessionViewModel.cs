using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using NewLibrarySpike.Data;
using NewLibrarySpike.Services;

namespace NewLibrarySpike.ViewModel
{
    [Export]
    class LibrarySessionViewModel : NavigationAwareViewModel
    {
        private SessionWithStrokes _session;

        [Import]
        Library Library { get; set; }

        [Import]
        StrokeManager StrokeManager { get; set; }

        protected override void OnNavigatedTo(NavigationParameters parameters)
        {
            var playerId = Guid.Parse((string)parameters["PlayerId"]);
            var time = DateTime.Parse((string)parameters["Time"]);
            var club = (string)parameters["ClubType"];
            var ball = (string)parameters["BallType"];
            Player = Library.GetPlayer(playerId);
            _session = Library.GetSessionsWithStrokes(Player.Id,filter: new LibraryFilter(club, ball, time).Apply).Single();
            UpdateStrokes();
        }

        private void UpdateStrokes()
        {
            var c = new SessionComparer();
            var loadedSession = StrokeManager.Sessions.FirstOrDefault(x => c.Equals(x, _session));

            Strokes = _session.Strokes.Select(s =>
            {
                var item = new LibraryStrokeViewModel(s);
                if (loadedSession == null)
                {
                    item.IsLoaded = false;
                }
                else
                {
                    item.IsLoaded = loadedSession.Strokes.Any(x => x.Id == s.Id);
                    item.Color = item.IsLoaded ? loadedSession.Color : Colors.Transparent;
                }

                return item;
            }).ToArray();
        }

        public DBPlayer Player { get; set; }

        public ICollection<LibraryStrokeViewModel> Strokes { get; private set; }

        public ICommand ToggleIsLoadedCommand { get { return new DelegateCommand<LibraryStrokeViewModel>(ToggleIsLoaded); } }

        private void ToggleIsLoaded(LibraryStrokeViewModel stroke)
        {
            if (stroke.IsLoaded)
            {
                StrokeManager.UnloadStrokes(new[] { stroke.Stroke });
            }
            else
            {
                StrokeManager.LoadStrokes(new[] { stroke.Stroke });
            }
            UpdateStrokes();
        }
    }
}