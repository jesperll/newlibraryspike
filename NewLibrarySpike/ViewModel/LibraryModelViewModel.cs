using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows.Data;
using Microsoft.Practices.Prism.Regions;
using NewLibrarySpike.Data;
using NewLibrarySpike.ViewModel.Filter;

namespace NewLibrarySpike.ViewModel
{
    [Export]
    class LibraryModelViewModel : LibraryBaseViewModel
    {
        protected override ICollectionView Query()
        {
            var results = (from s in Filter.FilterFunc(Database.Strokes)
                           where s.IsModel
                           join p in Database.Players on s.PlayerId equals p.Id
                           select new ModelShotViewModel(s.Id, p, s.ClubType, s.Time.Date)).ToArray();
            var view = CollectionViewSource.GetDefaultView(results);
            view.Filter += FilterByName;
            if (Filter.GroupByDate)
            {
                view.SortDescriptions.Add(new SortDescription("Group", ListSortDirection.Ascending));
                view.SortDescriptions.Add(new SortDescription("Player.Name", ListSortDirection.Ascending));
                view.GroupDescriptions.Add(new PropertyGroupDescription("Group"));
            }
            else
            {
                view.SortDescriptions.Add(new SortDescription("Player.Name", ListSortDirection.Ascending));
            }
            return view;
        }

        private bool FilterByName(object obj)
        {
            var p = (ModelShotViewModel)obj;
            return Library.NameMatchesFilter(p.Player.Name, FilterText);
        }

        public class ModelShotViewModel
        {
            private readonly Guid _strokeId;
            private readonly DBPlayer _player;
            private readonly string _clubType;
            private readonly DateTime _date;

            public ModelShotViewModel(Guid strokeId, DBPlayer player, string clubType, DateTime date)
            {
                _strokeId = strokeId;
                _player = player;
                _clubType = clubType;
                _date = date;
            }

            public string ClubType
            {
                get { return _clubType; }
            }

            public string Group
            {
                get { return _date.ToShortDateString(); }
            }

            public DBPlayer Player
            {
                get { return _player; }
            }
        }
    }
}