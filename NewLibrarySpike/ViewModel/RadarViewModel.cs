using System.ComponentModel.Composition;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Prism.Regions;
using NewLibrarySpike.Views;

namespace NewLibrarySpike.ViewModel
{
    [Export]
    public class RadarViewModel : NavigationAwareViewModel
    {
        [Import]
        RadarManager RadarManager { get; set; }

        public ICommand ConnectCommand
        {
            get
            {
                return new DelegateCommand(() =>
                    {
                        RadarManager.IsConnected = true;
                        NavigateCommand.Execute("SessionsView");
                    });
            }
        }
    }

    [Export]
    public class RadarManager : BindableBase
    {
        public bool IsConnected { get; set; }
    }

}