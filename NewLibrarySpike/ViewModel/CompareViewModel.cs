using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Windows.Ink;
using Microsoft.Practices.Prism.Regions;
using NewLibrarySpike.Data;
using NewLibrarySpike.Services;

namespace NewLibrarySpike.ViewModel
{
    [Export]
    class CompareViewModel : NavigationAwareViewModel
    {
        [Import]
        StrokeManager StrokeManager { get; set; }

        [Import]
        Library Library { get; set; }

        protected override void OnNavigatedTo(NavigationParameters parameters)
        {
            var strokeA = StrokeManager.SelectedStrokes.FirstOrDefault();
            var playerA = Library.GetPlayer(strokeA.PlayerId);
            var session = new SessionWithStrokes
            {
                Time = strokeA.Time.LocalDateTime.Date,
                Player=playerA,
                ClubType = strokeA.ClubType,
                BallType = strokeA.BallType,
                Strokes = new[] { strokeA },
            };
            var c = new SessionComparer();
            var ses = StrokeManager.Sessions.First(s => c.Equals(s, session));
            session.Color = ses.Color;
            var index = ses.Strokes.OrderBy(x => x.Time).IndexOf(strokeA.Id, x => x.Id);
            SessionA = session;
            StrokeA = new StrokeViewModel(strokeA, index + 1);
            //StrokeManager.IsCompareMode = true;
        }

        public SessionWithStrokes SessionA { get; set; }
        public StrokeViewModel StrokeA { get; set; }
    }

    public static class EnumerableMethods
    {
        public static int IndexOf<T>(this IEnumerable<T> source, T value, IEqualityComparer<T> comparer)
        {
            var index = 0;
            foreach (var item in source)
            {
                if (comparer.Equals(item, value))
                    return index;
                index++;
            }
            return -1;

        }

        public static int IndexOf<T>(this IEnumerable<T> source, T value)
        {
            return IndexOf(source, value, EqualityComparer<T>.Default);
        }

        public static int IndexOf<T, TValue>(this IEnumerable<T> source, TValue value, Func<T, TValue> selector,
            IEqualityComparer<TValue> comparer)
        {
            return IndexOf(source.Select(selector), value, comparer);
        }

        public static int IndexOf<T, TValue>(this IEnumerable<T> source, TValue value, Func<T, TValue> selector)
        {
            return IndexOf(source.Select(selector), value, EqualityComparer<TValue>.Default);
        }
    }
}