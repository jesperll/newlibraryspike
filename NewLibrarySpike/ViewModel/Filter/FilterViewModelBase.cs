using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;

namespace NewLibrarySpike.ViewModel.Filter
{
    abstract class FilterViewModelBase : BindableBase
    {
        private readonly string _name;

        protected FilterViewModelBase(string name)
        {
            _name = name;
            ClearCommand = new DelegateCommand(Clear);
        }

        public ICommand ClearCommand { get; private set; }
        protected virtual void Clear() { }

        public string Name
        {
            get { return _name; }
        }
    }
}