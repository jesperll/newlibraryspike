using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Mvvm;
using NewLibrarySpike.Data;

namespace NewLibrarySpike.ViewModel.Filter
{
    [Export]
    class FilterViewModel : BindableBase
    {
        public FilterViewModel()
        {
            Clubs = new ClubFilterViewModel();
            Clubs.FilterChanged += (sender, args) => OnFilterChanged();
            Video = new VideoFilterViewModel();
            Video.FilterChanged += (sender, args) => OnFilterChanged();
            PropertyChanged += (sender, args) => OnFilterChanged();
        }

        public event EventHandler FilterChanged;

        void OnFilterChanged()
        {
            var handler = FilterChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }


        public FilterViewModelBase[] Filters { get; private set; }

        public VideoFilterViewModel Video { get; set; }
        public ClubFilterViewModel Clubs { get; set; }
        public bool Starred { get; set; }
        public bool GroupByDate { get; set; }

        public IQueryable<DBStroke> FilterFunc(IQueryable<DBStroke> input)
        {
            var videoCount = new[] { Video.Above , Video.FaceOn , Video.TargetLine }.Count(y => y);
            if (videoCount > 0)
                input = input.Where(x => x.VideoCount > videoCount);
            else if (Video.Any)
                input = input.Where(x => x.VideoCount > 0);

            if (Starred)
                input = input.Where(x => x.IsStarred);
            var clubs = new HashSet<string>(
                from g in Clubs.ClubTypeGroups
                from c in g.ClubTypes
                where g.IsSelected || c.IsSelected
                select c.ShortName);
            if (clubs.Any())
                input = input.Where(x => clubs.Contains(x.ClubType));
            return input;
        }
    }
}