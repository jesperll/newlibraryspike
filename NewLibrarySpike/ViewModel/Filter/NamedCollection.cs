using System.Collections.ObjectModel;

namespace NewLibrarySpike.ViewModel.Filter
{
    class NamedCollection<T> : Collection<T>
    {
        private readonly string _name;

        public NamedCollection(string name)
        {
            _name = name;
        }

        public string Name
        {
            get { return _name; }
        }
    }
}