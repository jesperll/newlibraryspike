using System;
using Microsoft.Practices.Prism.Mvvm;

namespace NewLibrarySpike.ViewModel.Filter
{
    class VideoFilterViewModel : BindableBase
    {
        private bool _faceOn;
        private bool _targetLine;
        private bool _above;
        private bool _any;

        public bool Any
        {
            get { return _any; }
            set
            {
                _any = value;
                if (value)
                {
                    _targetLine = false;
                    _faceOn = false;
                    _above = false;
                    OnPropertyChanged("");
                }
                OnFilterChanged();
            }
        }

        public event EventHandler FilterChanged;
        public bool IsActive { get; private set; }
        protected virtual void OnFilterChanged()
        {
            IsActive = Any || FaceOn || TargetLine || Above;
            var handler = FilterChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        public bool FaceOn
        {
            get { return _faceOn; }
            set
            {
                _faceOn = value;
                if (value)
                {
                    _any = false;
                    OnPropertyChanged("Any");
                }
                OnFilterChanged();
            }
        }

        public bool TargetLine
        {
            get { return _targetLine; }
            set
            {
                _targetLine = value;
                if (value)
                {
                    _any = false;
                    OnPropertyChanged("Any");
                }
                OnFilterChanged();
            }
        }

        public bool Above
        {
            get { return _above; }
            set
            {
                _above = value;
                if (value)
                {
                    _any = false;
                    OnPropertyChanged("Any");
                }
                OnFilterChanged();
            }
        }
    }
}