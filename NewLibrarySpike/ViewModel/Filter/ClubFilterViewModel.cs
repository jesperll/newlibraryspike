using System;
using System.Collections.Generic;
using System.Linq;
using NewLibrarySpike.Model;

namespace NewLibrarySpike.ViewModel.Filter
{
    class ClubFilterViewModel : FilterViewModelBase
    {
        public ClubFilterViewModel()
            : base("Clubs")
        {
            ClubTypeGroups = new[]
            {
                new NamedCollection<ClubType>("Woods")
                {   new ClubType("Driver","Dr"),
                    new ClubType("2 Wood","2w"),
                    new ClubType("3 Wood","3w"),
                    new ClubType("4 Wood","4w"),
                    new ClubType("5 Wood","5w"),
                    new ClubType("6 Wood","6w"),
                    new ClubType("7 Wood","7w"),
                    new ClubType("8 Wood","8w"),
                    new ClubType("9 Wood","9w")
                },
                new NamedCollection<ClubType>("Irons")
                {
                    new ClubType("1 Iron","1i"),
                    new ClubType("2 Iron","2i"),
                    new ClubType("3 Iron","3i"),
                    new ClubType("4 Iron","4i"),
                    new ClubType("5 Iron","5i"),
                    new ClubType("6 Iron","6i"),
                    new ClubType("7 Iron","7i"),
                    new ClubType("8 Iron","8i"),
                    new ClubType("9 Iron","9i"),
                },
            
                new NamedCollection<ClubType>("Hybrids")
                {
                    new ClubType("1 Hybrid","1h"),
                    new ClubType("2 Hybrid","2h"),
                    new ClubType("3 Hybrid","3h"),
                    new ClubType("4 Hybrid","4h"),
                    new ClubType("5 Hybrid","5h"),
                    new ClubType("6 Hybrid","6h"),
                    new ClubType("7 Hybrid","7h"),
                    new ClubType("8 Hybrid","8h"),
                    new ClubType("9 Hybrid","9h"),
                },
                    new NamedCollection<ClubType>("Wedges")
                {
                    new ClubType("Pitching Wedge","PW"),
                    new ClubType("Sand Wedge","SW"),
                    new ClubType("Lob Wedge","LW"),
                    new ClubType("50� Wedge","50�"),
                    new ClubType("52� Wedge","52�"),
                    new ClubType("54� Wedge","54�"),
                    new ClubType("56� Wedge","56�"),
                    new ClubType("58� Wedge","58�"),
                    new ClubType("60� Wedge","60�"),
                    // and some more
                },
            }.Select(x => new ClubTypeCollectionViewModel(this, x)).ToArray();
        }



        protected override void Clear()
        {
            foreach (var @group in ClubTypeGroups)
            {
                group.Unselect();

            }
            OnFilterChanged();
        }
        public bool IsFiltered { get; private set; }
        public event EventHandler FilterChanged;

        public void OnFilterChanged()
        {
            IsFiltered = ClubTypeGroups.Any(g => g.IsSelected || g.ClubTypes.Any(c => c.IsSelected));
            var handler = FilterChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        public IList<ClubTypeCollectionViewModel> ClubTypeGroups { get; private set; }
    }
}