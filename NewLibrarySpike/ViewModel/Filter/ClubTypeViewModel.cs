using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using NewLibrarySpike.Model;

namespace NewLibrarySpike.ViewModel.Filter
{
    class ClubTypeViewModel : BindableBase
    {
        private readonly ClubType _club;

        public ClubTypeViewModel(ClubTypeCollectionViewModel parent, ClubFilterViewModel filter, ClubType club)
        {
            _club = club;
            SelectCommand = new DelegateCommand(
                () =>
                {
                    IsSelected = !IsSelected;
                    if (IsSelected)
                        parent.Unselect(false);
                    filter.OnFilterChanged();
                });
        }
        public void Unselect()
        {
            IsSelected = false;
        }
        public bool IsSelected { get; private set; }
        public ICommand SelectCommand { get; private set; }

        public string Name { get { return _club.Name; } }
        public string ShortName { get { return _club.ShortName; } }
    }
}