using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using NewLibrarySpike.Model;

namespace NewLibrarySpike.ViewModel.Filter
{
    class ClubTypeCollectionViewModel : BindableBase
    {
        private readonly string _name;

        public ClubTypeCollectionViewModel(ClubFilterViewModel filter, NamedCollection<ClubType> clubs)
        {
            _name = clubs.Name;
            ClubTypes = clubs.Select(club => new ClubTypeViewModel(this, filter, club)).ToArray();
            SelectCommand = new DelegateCommand(
                () =>
                {
                    IsSelected = !IsSelected;
                    if (IsSelected)
                        foreach (var c in ClubTypes)
                            c.Unselect();
                    filter.OnFilterChanged();
                });
        }

        public string Name
        {
            get { return _name; }
        }

        public IList<ClubTypeViewModel> ClubTypes { get; private set; }

        public bool IsSelected { get; private set; }

        public ICommand SelectCommand { get; private set; }

        public void Unselect(bool recurse=true)
        {
            IsSelected = false;
            if (!recurse)
                return;
            foreach (var club in ClubTypes)
            {
                club.Unselect();
            }
        }
    }
}