using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using NewLibrarySpike.Data;

namespace NewLibrarySpike.ViewModel
{
    [Export]
    class LibrarySearchViewModel : LibraryBaseViewModel
    {
        protected override ICollectionView Query()
        {
            var results = Library.GetPlayersWithStrokeCounts(Filter.FilterFunc, Filter.GroupByDate);
            var view = CollectionViewSource.GetDefaultView(results);
            view.Filter += FilterByName;
            if (Filter.GroupByDate)
                view.GroupDescriptions.Add(new PropertyGroupDescription("Group"));
            PlayerCount = results.Count;
            StrokeCount = results.Sum(x => x.StrokeCount);

            ModelStrokeCount = Filter.FilterFunc(Database.Strokes).Count(x => x.IsModel);
            return view;
        }

        private bool FilterByName(object obj)
        {
            var p = (PlayerWithStrokeCount)obj;
            return Library.NameMatchesFilter(p.Player.Name, FilterText);
        }

        public int ModelStrokeCount { get; private set; }
        public int PlayerCount { get; private set; }
        public int StrokeCount { get; private set; }

        public ICommand ShowPlayerLibraryCommand { get { return new DelegateCommand<PlayerWithStrokeCount>(ShowPlayerLibrary); } }

        private void ShowPlayerLibrary(PlayerWithStrokeCount player)
        {
            if (player.Player.Id != Guid.Empty)
            {
                var nav = new NavigationParameters { { "PlayerId", player.Player.Id } };
                NavigationService.RequestNavigate(new Uri("/LibraryPlayerSessionsView" + nav, UriKind.Relative));
            }
        }

        public ICommand ShowModelLibraryCommand
        {
            get { return new DelegateCommand(ShowModelLibrary); }
        }

        private void ShowModelLibrary()
        {
            NavigationService.RequestNavigate(new Uri("/LibraryModelView", UriKind.Relative));
        }
    }
}