using System.Windows.Media;
using NewLibrarySpike.Data;

namespace NewLibrarySpike.ViewModel
{
    class LibraryPlayerSessionViewModel
    {
        private readonly SessionWithStrokes _session;

        public LibraryPlayerSessionViewModel(SessionWithStrokes session)
        {
            _session = session;
        }

        public Color Color { get; set; }
        public bool? IsLoaded { get; set; }
        public string Group { get { return _session.Time.ToShortDateString(); } }

        public SessionWithStrokes Session
        {
            get { return _session; }
        }
    }
}