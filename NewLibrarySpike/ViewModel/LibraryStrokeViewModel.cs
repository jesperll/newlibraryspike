using System.Windows.Media;
using NewLibrarySpike.Data;

namespace NewLibrarySpike.ViewModel
{
    class LibraryStrokeViewModel
    {
        private readonly DBStroke _stroke;

        public LibraryStrokeViewModel(DBStroke stroke)
        {
            _stroke = stroke;
        }

        public Color Color { get; set; }
        public bool IsLoaded { get; set; }

        public DBStroke Stroke
        {
            get { return _stroke; }
        }

        public string Data { get { return string.Format("{0:0.0}", _stroke.DataValue); } }
    }
}