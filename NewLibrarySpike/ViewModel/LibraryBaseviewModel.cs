using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using NewLibrarySpike.Data;
using NewLibrarySpike.Services;
using NewLibrarySpike.ViewModel.Filter;

namespace NewLibrarySpike.ViewModel
{
    abstract class LibraryBaseViewModel : NavigationAwareViewModel
    {
        [Import]
        protected IDatabase Database { get; private set; }
        [Import]
        protected Library Library { get; private set; }

        [Import]
        protected StrokeManager StrokeManager { get; private set; }

        [Import]
        public FilterViewModel Filter { get; private set; }

        public ICollectionView Results { get; protected set; }

        private string _filterText;
        public string FilterText
        {
            get { return _filterText; }
            set
            {
                _filterText = value;
                CollectionViewSource.GetDefaultView(Results).Refresh();
            }
        }

        protected override void OnNavigatedTo(NavigationParameters parameters)
        {
            Filter.FilterChanged += OnFilterChanged;
            DoQuery();
        }

        public override void OnNavigatedFrom(NavigationContext navigationContext)
        {
            Filter.FilterChanged -= OnFilterChanged;
        }

        private void OnFilterChanged(object sender, EventArgs e)
        {
            DoQuery();
        }

        private void DoQuery()
        {
            Results = Query();
        }

        protected abstract ICollectionView Query();

        public ICommand ClearFilterTextCommand
        {
            get { return new DelegateCommand(() => FilterText = ""); }
        }


    }
}