using System.ComponentModel.Composition;

namespace NewLibrarySpike.ViewModel
{
    [Export]
    class StartViewModel: NavigationAwareViewModel{}
}