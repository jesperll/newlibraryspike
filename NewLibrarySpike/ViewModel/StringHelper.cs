using System;

namespace NewLibrarySpike.ViewModel
{
    public static class StringHelper
    {
        public static string Pluralize(this int count, string singular, string plural)
        {
            if (count < 0)
                throw new ArgumentOutOfRangeException();
            return string.Format(count == 1 ? singular : plural, count);
        }

    }
}