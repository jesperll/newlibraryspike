﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;

namespace NewLibrarySpike.ViewModel
{
    public class NavigationAwareViewModel : BindableBase, INavigationAware, IPartImportsSatisfiedNotification
    {
        [Import]
        protected IEventAggregator EventAggregator { get; private set; }

        protected IRegionNavigationService NavigationService { get; private set; }

        void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            NavigationService = navigationContext.NavigationService;
            ((NavigationAwareViewModel)this).OnNavigatedTo(navigationContext.Parameters);
        }

        protected virtual void OnNavigatedTo(NavigationParameters parameters)
        {
        }

        public virtual bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public virtual void OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public virtual void OnImportsSatisfied()
        {
        }

        public ICommand NavigateCommand { get { return new DelegateCommand<string>(Navigate); } }

        private void Navigate(string target)
        {
            NavigationService.RequestNavigate(target);
        }

        public ICommand GoBackCommand { get { return new DelegateCommand(GoBack); } }

        protected void GoBack()
        {
            NavigationService.Journal.GoBack();
        }

        public bool IsIndesignMode { get { return IsInDesignModeLazy.Value; } }
        static readonly Lazy<bool> IsInDesignModeLazy = new Lazy<bool>(() =>
            (bool)DependencyPropertyDescriptor.FromProperty(
                DesignerProperties.IsInDesignModeProperty,
                typeof(FrameworkElement))
                .Metadata
                .DefaultValue);

    }
}