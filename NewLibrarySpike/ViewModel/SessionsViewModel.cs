using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Ink;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using NewLibrarySpike.Data;
using NewLibrarySpike.Services;

namespace NewLibrarySpike.ViewModel
{
    [Export]
    class SessionsViewModel : NavigationAwareViewModel
    {
        [Import]
        IDatabase Database { get; set; }

        [Import]
        StrokeManager StrokeManager { get; set; }

        [Import]
        Library Library { get; set; }

        [Import]
        RadarManager RadarManager { get; set; }

        public ICommand ShowLibraryCommand { get { return new DelegateCommand(ShowLibrary); } }
        public ICommand ShowPlayerLibraryCommand { get { return new DelegateCommand<object>(ShowPlayerLibrary); } }

        private void ShowPlayerLibrary(object playerId)
        {
            if (!Equals(playerId, Guid.Empty))
            {
                var nav = new NavigationParameters { { "PlayerId", playerId } };
                NavigationService.RequestNavigate(new Uri("/LibraryPlayerSessionsView" + nav, UriKind.Relative));
            }
        }

        [Import]
        IRegionManager RegionManager { get; set; }

        private void ShowLibrary()
        {
            NavigationService.RequestNavigate(new Uri("/LibrarySearchView", UriKind.Relative));
        }

        public IList<SessionViewModel> Sessions { get; private set; }

        public ICommand SelectSessionCommand
        {
            get { return new DelegateCommand<SessionViewModel>(SelectSession); }
        }

        public ICommand ShowSessionStrokesCommand
        {
            get { return new DelegateCommand<SessionViewModel>(ShowSessionStrokes); }
        }

        public ICommand UnloadSessionCommand
        {
            get { return new DelegateCommand<SessionViewModel>(UnloadSession); }
        }

        private void UnloadSession(SessionViewModel session)
        {
            StrokeManager.UnloadStrokes(session.Strokes);
        }

        private void SelectSession(SessionViewModel session)
        {
            StrokeManager.SetSelection(session.Strokes.Where(x => !x.Ignore));
            foreach (var viewModel in Sessions)
            {
                viewModel.IsSelected = viewModel == session;
            }
        }

        private void ShowSessionStrokes(SessionViewModel session)
        {
            var nav = new NavigationParameters
            {
                {"PlayerId", session.Session.Player.Id},
                {"Time", session.Session.Time},
                {"ClubType", session.Session.ClubType},
                {"BallType", session.Session.BallType},
            };
            NavigationService.RequestNavigate(new Uri("/StrokesView" + nav, UriKind.Relative));
        }

        protected override void OnNavigatedTo(NavigationParameters parameters)
        {
            StrokeManager.ShowAll = true;
            StrokeManager.PropertyChanged += StrokeManagerOnPropertyChanged;
            Refresh();

            var selectedSession = Sessions.FirstOrDefault(x => StrokeManager.SelectedStrokes.Intersect(x.Strokes).Any());
            if (selectedSession != null)
                SelectSession(selectedSession);
            else
                StrokeManager.ClearSelection();
            IsRadarConnected = RadarManager.IsConnected;
        }

        public override void OnNavigatedFrom(NavigationContext navigationContext)
        {
            StrokeManager.PropertyChanged -= StrokeManagerOnPropertyChanged;
        }

        private void StrokeManagerOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            Refresh();
        }

        private void Refresh()
        {
            Sessions = StrokeManager.Sessions.Select(x => new SessionViewModel(x)).ToArray();

        }

        private DBStroke[] GetSelectedStrokes()
        {
            return Sessions.Where(x => x.IsSelected).SelectMany(x => x.Strokes).ToArray();
        }

        public ICommand UnloadStrokesCommand
        {
            get { return new DelegateCommand(UnloadStrokes); }
        }

        private void UnloadStrokes()
        {
            var strokes = GetSelectedStrokes();

            StrokeManager.UnloadStrokes(strokes);
        }

        public ICommand ModifyStrokesCommand
        {
            get { return new DelegateCommand(ModifyStrokes); }
        }

        public bool IsRadarConnected { get; set; }

        public ICommand UnloadAllCommand
        {
            get { return new DelegateCommand(() =>
            {
                StrokeManager.ClearSelection();
                StrokeManager.UnloadStrokes(StrokeManager.Strokes.ToArray());
            }); }
        }


        private void ModifyStrokes()
        {
            var strokes = GetSelectedStrokes();

            MessageBox.Show(strokes.Length.Pluralize("Modifying 1 stroke", "Modifying {0} strokes"));
        }

        public void SelectNext()
        {
            var i = Sessions.IndexOf(true, x => x.IsSelected);
            if (i > 0)
                SelectSession(Sessions[i - 1]);

        }

        public void SelectPrevious()
        {
            var i = Sessions.IndexOf(true, x => x.IsSelected);
            if (i < Sessions.Count - 1)
                SelectSession(Sessions[i + 1]);

        }


    }
}