﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;

namespace NewLibrarySpike.ViewModel
{
    [Export]
    public class SidebarViewModel : NavigationAwareViewModel
    {
        [Import]
        protected IRegionManager RegionManager { get; private set; }
        public ICommand GoHomeCommand { get { return new DelegateCommand(GoHome); } }

        protected void GoHome()
        {
            RegionManager.RequestNavigate(RegionNames.SidebarContentRegion, new Uri("/StartView", UriKind.Relative));
        }


    }
}