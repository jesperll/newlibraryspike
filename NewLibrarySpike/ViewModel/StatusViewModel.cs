using System.ComponentModel.Composition;
using System.Linq;
using System.Windows.Ink;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Prism.PubSubEvents;
using NewLibrarySpike.Services;

namespace NewLibrarySpike.ViewModel
{
    [Export]
    class StatusViewModel : BindableBase, IPartImportsSatisfiedNotification
    {
        [Import]
        IEventAggregator EventAggregator { get; set; }

        [Import]
        public StrokeManager StrokeManager { get; set; }

        public void OnImportsSatisfied()
        {
            EventAggregator.GetEvent<LoadedStrokesChangedEvent>().Subscribe(@event => OnPropertyChanged("Status"));
            StrokeManager.PropertyChanged += (sender, args) => OnPropertyChanged(null);
        }

        public string Status
        {
            get
            {
                return string.Format("{0} with {1} are loaded", StrokeManager.Sessions.Count.Pluralize("1 group", "{0} groups"), StrokeManager.Strokes.Count().Pluralize("1 stroke", "{0} strokes"));
            }
        }

        public string Dispersion
        {
            get
            {
                if (StrokeManager.ShowAll || StrokeManager.SelectedStrokes.Count < 1)
                    return string.Format("Show points and ellipses for {0} in {1}",
                        StrokeManager.Strokes.Count(x => !x.Ignore).Pluralize("1 stroke", "{0} strokes"),
                        StrokeManager.Sessions.Count.Pluralize("1 group", "{0} groups"));
                if (StrokeManager.ShowAverages)
                {
                    return StrokeManager.SelectedStrokes.Count.Pluralize(
                        "Show a single point with a small circle around",
                        "Show {0} points and corresponding ellipse");
                }
                var sessionForSelection =
                    StrokeManager.Sessions.First(x => x.Strokes.Intersect(StrokeManager.SelectedStrokes).Any());
                return string.Format("Show {0} points and corresponding ellipse. Highlight selected stroke.", sessionForSelection.Strokes.Count(x => !x.Ignore));
            }
        }
        public string Trajectory
        {
            get
            {
                return StrokeManager.SelectedStrokes.Count < 1 ? "Show nothing" :
                    StrokeManager.SelectedStrokes.Count.Pluralize("Show trajectory for the selected stroke", "Show {0} trajectories");
            }
        }
        public string Video
        {
            get
            {
                if (StrokeManager.ShowAverages || StrokeManager.SelectedStrokes.Count < 1)
                    return "Show nothing";
                var videoCount = StrokeManager.SelectedStrokes.First().VideoCount;
                return videoCount == 0 ? "The selected stroke has no videos" :
                    string.Format("Show {0} for the selected stroke", videoCount.Pluralize("video", "{0} videos"));
            }
        }
        public string Numerics
        {
            get
            {
                if (StrokeManager.SelectedStrokes.Count < 1)
                    return "Show nothing";
                var value = StrokeManager.SelectedStrokes.Average(x => x.DataValue);
                return (StrokeManager.ShowAverages && StrokeManager.SelectedStrokes.Count > 1 ?
                    "Show average value for selected strokes" :
                    "Show value for the selected stroke") + string.Format(": {0:0.0}", value);
            }
        }
        public string ClubView
        {
            get
            {
                if (StrokeManager.SelectedStrokes.Count < 1)
                    return "Show nothing";
                return StrokeManager.ShowAverages && StrokeManager.SelectedStrokes.Count > 1 ?
                    "Show average values for selected strokes" :
                    "Show values for the selected stroke";
            }
        }
        public string Optimizer
        {
            get
            {
                if (StrokeManager.SelectedStrokes.Count < 1)
                    return "Show nothing";
                return StrokeManager.ShowAverages && StrokeManager.SelectedStrokes.Count > 1 ? "Show optimized values for average of selected strokes" : "Show optimized values for the selected stroke";
            }
        }
    }
}