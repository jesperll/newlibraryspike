using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows.Data;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using NewLibrarySpike.Data;
using NewLibrarySpike.Services;
using NewLibrarySpike.ViewModel.Filter;

namespace NewLibrarySpike.ViewModel
{
    [Export]
    class LibraryPlayerSessionsViewModel : NavigationAwareViewModel
    {
        private Guid _playerId;

        [Import]
        Library Library { get; set; }

        [Import]
        StrokeManager StrokeManager { get; set; }

        [Import]
        public FilterViewModel Filter { get; private set; }

        public LibraryPlayerSessionsViewModel()
        {
            if (!IsIndesignMode)
                return;
            var db = new SampleData();
            StrokeManager = new StrokeManager();
            Library = new Library() { Database = db };
            Filter = new FilterViewModel();
            _playerId = db.Players.First().Id;
            Player = Library.GetPlayerWithStrokeCount(_playerId);
            FilterSessions();
        }

        protected override void OnNavigatedTo(NavigationParameters parameters)
        {
            _playerId = Guid.Parse((string)parameters["PlayerId"]);
            Player = Library.GetPlayerWithStrokeCount(_playerId);
            Filter.FilterChanged += OnFilterChanged;
            FilterSessions();
        }

        private void OnFilterChanged(object sender, EventArgs eventArgs)
        {
            FilterSessions();
        }

        public PlayerWithStrokeCount Player { get; private set; }

        private void FilterSessions()
        {
            PlayerSessions = Library.GetSessionsWithStrokes(_playerId, Filter.FilterFunc).Select(s =>
            {
                var item = new LibraryPlayerSessionViewModel(s);
                var c = new SessionComparer();
                var loadedSession = StrokeManager.Sessions.FirstOrDefault(x => c.Equals(x, s));
                if (loadedSession == null)
                {
                    item.IsLoaded = false;
                }
                else
                {
                    item.IsLoaded = loadedSession.Strokes.Count == s.Strokes.Count ? true : default(bool?);
                    item.Color = loadedSession.Color;
                }
                return item;
            }).ToArray();
            if (Filter.GroupByDate)
                CollectionViewSource.GetDefaultView(PlayerSessions).GroupDescriptions.Add(new PropertyGroupDescription("Group"));
        }

        public ICommand ToggleIsLoadedCommand { get { return new DelegateCommand<LibraryPlayerSessionViewModel>(ToggleIsLoaded); } }

        private void ToggleIsLoaded(LibraryPlayerSessionViewModel session)
        {
            switch (session.IsLoaded)
            {
                case true:
                    StrokeManager.UnloadStrokes(session.Session.Strokes);
                    StrokeManager.ClearSelection();
                    break;
                case false:
                    StrokeManager.LoadStrokes(session.Session.Strokes);
                    StrokeManager.SetSelection(session.Session.Strokes);
                    break;
                case null:
                    StrokeManager.LoadStrokes(session.Session.Strokes);
                    StrokeManager.SetSelection(session.Session.Strokes);
                    break;
            }
            FilterSessions();
        }


        public ICollection<LibraryPlayerSessionViewModel> PlayerSessions { get; private set; }

        public ICommand ShowSessionCommand
        {
            get { return new DelegateCommand<LibraryPlayerSessionViewModel>(ShowSession); }
        }


        private void ShowSession(LibraryPlayerSessionViewModel session)
        {
            StrokeManager.LoadStrokes(session.Session.Strokes);
            StrokeManager.SetSelection(session.Session.Strokes);

            NavigationService.RequestNavigate(new Uri("/SessionsView", UriKind.Relative));

            var nav = new NavigationParameters
            {
                {"PlayerId", session.Session.Player.Id},
                {"Time", session.Session.Time},
                {"ClubType", session.Session.ClubType},
                {"BallType", session.Session.BallType},
            };
            NavigationService.RequestNavigate(new Uri("/StrokesView" + nav, UriKind.Relative));
        }
    }
}