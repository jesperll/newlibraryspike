﻿using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace NewLibrarySpike.Views
{
    /// <summary>
    /// Interaction logic for TestManagerView.xaml
    /// </summary>
    [Export("TestManagerView")]
    public partial class TestManagerView : UserControl
    {
        public TestManagerView()
        {
            InitializeComponent();
        }
    }
}
