﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using NewLibrarySpike.ViewModel;

namespace NewLibrarySpike.Views
{
    [Export("LibraryPlayerSessionsView")]
    public partial class LibrarySessionsView : UserControl
    {
        public LibrarySessionsView()
        {
            InitializeComponent();
        }

        [Import]
        LibraryPlayerSessionsViewModel ViewModel
        {
            set { this.DataContext = value; }
        }

    }
}
