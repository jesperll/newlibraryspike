﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using NewLibrarySpike.ViewModel;

namespace NewLibrarySpike.Views
{
    [Export("LibrarySessionView")]
    public partial class LibrarySessionView : UserControl
    {
        public LibrarySessionView()
        {
            InitializeComponent();
        }

        [Import]
        LibrarySessionViewModel ViewModel { set { DataContext = value; } }
    }
}
