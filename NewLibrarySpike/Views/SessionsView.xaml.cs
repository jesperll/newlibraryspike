﻿using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using NewLibrarySpike.ViewModel;

namespace NewLibrarySpike.Views
{
    [Export("SessionsView")]
    public partial class SessionsView : UserControl
    {
        public SessionsView()
        {
            InitializeComponent();
        }

        [Import]
        SessionsViewModel ViewModel
        {
            set { this.DataContext = value; }
            get { return (SessionsViewModel)DataContext; }
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Right:
                    var vm = ViewModel.Sessions.FirstOrDefault(x => x.IsSelected);
                    if (vm != null)
                        ViewModel.ShowSessionStrokesCommand.Execute(vm);
                    break;
                case Key.Up:
                    ViewModel.SelectNext();
                    break;
                case Key.Down:
                    ViewModel.SelectPrevious();
                    break;
                default:
                    base.OnPreviewKeyDown(e);
                    break;
            }
        }

    }
}
