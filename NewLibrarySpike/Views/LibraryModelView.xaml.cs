﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using System.Windows.Input;
using NewLibrarySpike.ViewModel;

namespace NewLibrarySpike.Views
{
    /// <summary>
    /// Interaction logic for LibraryModelView.xaml
    /// </summary>
    [Export("LibraryModelView")]
    public partial class LibraryModelView : UserControl
    {
        public LibraryModelView()
        {
            InitializeComponent();
            Loaded += (sender, args) => Dispatcher.BeginInvoke(new Action(() =>
            {
                FilterTextBox.Focus();
                FilterTextBox.SelectAll();
            }));
        }


        [Import]
        LibraryModelViewModel ViewModel
        {
            set { this.DataContext = value; }
        }

        private void FilterTextBox_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                FilterTextBox.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                FilterTextBox.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
            }
        }
    }
}
