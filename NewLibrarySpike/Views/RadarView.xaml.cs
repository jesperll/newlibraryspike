﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using NewLibrarySpike.ViewModel;

namespace NewLibrarySpike.Views
{
    /// <summary>
    /// Interaction logic for RadarView.xaml
    /// </summary>
    [Export("RadarView")]
    public partial class RadarView : UserControl
    {
        public RadarView()
        {
            InitializeComponent();
        }

        [Import]
        RadarViewModel ViewModel
        {
            get { return DataContext as RadarViewModel; }
            set { DataContext = value; }
        }
    }

}
