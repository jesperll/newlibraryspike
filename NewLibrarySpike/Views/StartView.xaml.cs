﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NewLibrarySpike.ViewModel;

namespace NewLibrarySpike.Views
{
    /// <summary>
    /// Interaction logic for StartView.xaml
    /// </summary>
    [Export("StartView")]
    public partial class StartView : UserControl
    {
        public StartView()
        {
            InitializeComponent();
        }

        [Import]
        private StartViewModel ViewModel
        {
            set { this.DataContext = value; }
            get { return (StartViewModel)DataContext; }
        }

    }
}
