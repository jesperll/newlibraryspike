﻿using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using NewLibrarySpike.ViewModel;

namespace NewLibrarySpike.Views
{
    [Export("StrokesView")]
    public partial class StrokesView : UserControl
    {
        public StrokesView()
        {
            InitializeComponent();            
        }

        [Import]
        private StrokesViewModel ViewModel
        {
            set { this.DataContext = value; }
            get { return (StrokesViewModel)DataContext; }
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Left:
                    ViewModel.GoBackCommand.Execute(null);
                    break;
                case Key.Up:
                    ViewModel.SelectNext();
                    break;
                case Key.Down:
                    ViewModel.SelectPrevious();
                    break;
                default:
                    base.OnPreviewKeyDown(e);
                    break;
            }
        }
    }
}
