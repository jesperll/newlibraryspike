﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NewLibrarySpike.ViewModel;

namespace NewLibrarySpike.Views
{
    /// <summary>
    /// Interaction logic for SidebarView.xaml
    /// </summary>
    [Export("SidebarView")]
    public partial class SidebarView : UserControl
    {
        public SidebarView()
        {
            InitializeComponent();
        }
        
        [Import]
        public SidebarViewModel ViewModel
        {
            get { return DataContext as SidebarViewModel; }
            set { DataContext = value; }
        }
    }
}
