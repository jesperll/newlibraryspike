﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NewLibrarySpike.ViewModel.Filter;

namespace NewLibrarySpike
{
    /// <summary>
    /// Interaction logic for FilterControl.xaml
    /// </summary>
    public partial class FilterControl : UserControl
    {
        public FilterControl()
        {
            InitializeComponent();
        }

        private void ClubButton_OnClick(object sender, RoutedEventArgs e)
        {
            ClubPopup.IsOpen = true;
        }

        private void VideoButton_OnClick(object sender, RoutedEventArgs e)
        {
            var vm = ((FilterViewModel) DataContext).Video;
            if (!vm.IsActive)
                vm.Any = true;
            VideoPopup.IsOpen = true;
        }
    }
}
